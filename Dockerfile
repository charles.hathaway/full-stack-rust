# Dockerfile for creating a statically-linked Rust application using docker's
# multi-stage build feature. This also leverages the docker build cache to avoid
# re-downloading dependencies if they have not changed.
FROM rust:latest AS build
WORKDIR /usr/src

# Download the target for static linking.
RUN rustup target add x86_64-unknown-linux-musl

WORKDIR /usr/src/
COPY common common
COPY back back

# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
WORKDIR /usr/src/back
RUN cargo build --release

# Copy the source and build the application.
RUN cargo install --path .

# Copy the statically-linked binary into a scratch container.
FROM ubuntu:latest
RUN apt update
RUN apt install -y libssl1.1
COPY --from=build /usr/local/cargo/bin/back .
USER 1000
EXPOSE 8080
ENTRYPOINT ["./back"]
