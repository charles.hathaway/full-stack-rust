# Full-stack Rust

The goal of this project is to write a full-stack Rust application that uses common definitions for shared data objects
but allows us to serve the front end seperately from the backend. In the end, our goal is to deploy something
to GCP.

To this end, we will make a chat app which establishes a connection to the server using web sockets and sends/recieves
messages.

## Getting started

To start, we run these 3 commands:

```
cargo new --bin back
cargo new --lib common
npm init rust-webpack front
```

Let's create our shared data structure in common/src/lib.rs. Make sure to include Serde, as that is how we will
be serializing/deserializing data.

## Build the backend

A simple HTTP server which opens a WS connection and passes messages to/from all connected peers

```
cd back
cargo run
```

## Run the front end

```
cd front
nvm use stable
npm install
npm run
```
