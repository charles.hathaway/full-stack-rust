#!/bin/bash

pushd back
rustup target add x86_64-unknown-linux-musl
cargo build --target x86_64-unknown-linux-musl --release
cp ./target/x86_64-unknown-linux-musl/release/back ../dist/
popd

pushd front
npm run-script build
popd

pushd dist
# Just verify the build
docker build .
gcloud app deploy
