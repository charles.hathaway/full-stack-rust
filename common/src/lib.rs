use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Message {
    Join(String),
    Leave(String),
    SendMessage(ChatMessage),
    ChangeUsername(ChangeUsername)
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ChatMessage {
    pub owner: String,
    pub content: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ChangeUsername {
    pub old_name: String,
    pub new_name: String,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
