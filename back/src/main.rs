use std::net::TcpListener;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Sender};
use std::thread::spawn;

use tungstenite::server::accept;
use tungstenite::protocol::{Role, WebSocket};
use tungstenite::Message as TMsg;

use common::Message;

struct Server {
    send: Vec<Sender<Message>>,
}

impl Server {
    fn new() -> Server {
        Server{send: vec!()}
    }

    fn add_client(&mut self, tx: Sender<Message>) {
        self.send.push(tx);
    }

    fn send_message(&mut self, msg: Message) {
        let mut to_remove = vec!();
        for (i, send) in self.send.iter().enumerate() {
            if let Err(_) = send.send(msg.clone()) {
                to_remove.push(i);
            }
        }

        for index in to_remove.iter().rev() {
            self.send.remove(*index);
        }
    }
}

fn main() {
    let server = Server::new();
    let server = Arc::new(Mutex::new(server));
    let conn = TcpListener::bind("0.0.0.0:8080").unwrap();
    for stream in conn.incoming() {
        println!("Got a connection!");
        let server = server.clone();
        spawn (move || {
            let stream = stream.unwrap();
            let mut recv_websocket = accept(stream.try_clone().unwrap()).unwrap();
            let mut send_websocket = WebSocket::from_partially_read(stream, vec!(), Role::Server, None);
            let (send, recv) = channel();
            server.lock().unwrap().add_client(send);
            // This is terrible, but OK for a proof of concept. Spin off another thread to write to
            // the client when it gets a message
            {
                spawn(move || {
                    while let Ok(msg) = recv.recv() {
                        println!("Sending message: {:?}", msg);
                        send_websocket.write_message(TMsg::Text(serde_json::to_string(&msg).unwrap())).unwrap();
                    }
                });
            }
            loop {
                let msg = recv_websocket.read_message().unwrap();


                // We do not want to send back ping/pong messages.
                if let TMsg::Text(text) = msg {
                    println!("Got message {}", text);
                    let msg: Message = serde_json::from_str(&text).unwrap();
                    server.lock().unwrap().send_message(msg);
                }
            }
        });
    }
}
