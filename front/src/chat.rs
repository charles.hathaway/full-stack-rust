#![recursion_limit = "128"]
use std::sync::{Arc, Mutex};
use wasm_bindgen::JsCast;
use wasm_bindgen::prelude::*;
use wasm_bindgen::closure::Closure;
use web_sys::{console, WebSocket};
use js_sys::Reflect::get;

use rand::Rng;
use yew::prelude::*;
use yew::callback::Callback;
use yew::{html, Component, ComponentLink, Html, InputData, ShouldRender};

use common::{Message, ChatMessage, ChangeUsername};

const nouns: [&str; 20] = [
    "glass",
    "wish",
    "lumber",
    "ground",
    "error",
    "whistle",
    "pocket",
    "low",
    "paper",
    "plantation",
    "plough",
    "channel",
    "baseball",
    "cows",
    "soap",
    "berry",
    "notebook",
    "pets",
    "mom",
    "lamp",
];

pub enum ModelMsg {
    Message(Message),
    Click,
    NewValue(String),
    NewUsername(String),
}

pub struct Model {
    link: Arc<Mutex<ComponentLink<Self>>>,
    messages: Vec<String>,
    onOpen: Closure<FnMut(JsValue)>,
    onMessage: Closure<FnMut(JsValue)>,
    onInput: Callback<InputData>,
    onUserChange: Callback<InputData>,
    onSubmit: Callback<web_sys::MouseEvent>,
    websocket: Arc<Mutex<WebSocket>>,
    value: String,
    username: String,
    prev_username: String,
}

impl Component for Model {
    type Message = ModelMsg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut rng = rand::thread_rng();
        let username = format!("{} {}", nouns[rng.gen_range(0, nouns.len())], nouns[rng.gen_range(0, nouns.len())]);
        let prev_username = username.clone();

        let link = Arc::new(Mutex::new(link));
        let mut websocket = WebSocket::new("wss://full-stack-rust.appspot.com/ws").unwrap();
        let mut websocket = Arc::new(Mutex::new(websocket));
        let onMessage = {
            let link = link.clone();
            Closure::wrap(Box::new(move |msgEvent| {
                let data = get(&msgEvent, &JsValue::from_str("data")).unwrap();
                let msg: Message = serde_json::from_str(&data.as_string().unwrap()).unwrap();
                link.lock().unwrap().send_message(ModelMsg::Message(msg.clone()));
                console::log_1(&JsValue::from_str(&format!("Hello world! Got message {:?}", msg)));
            }) as Box<dyn FnMut(JsValue)>)
        };


        let onOpen = {
            let websocket = websocket.clone();
            let username = username.clone();
            Closure::wrap(Box::new(move |msgEvent| {
                let msg = serde_json::to_string(&Message::Join(username.clone())).unwrap();
                console::log_1(&JsValue::from_str(&format!("Sending message {:?}", msg)));
                websocket.lock().unwrap().send_with_str(&msg).unwrap();
            }) as Box<dyn FnMut(JsValue)>)
        };

        let onInput = link.lock().unwrap().callback(|e: InputData| {
            ModelMsg::NewValue(e.value)
        });

        let onUserChange = link.lock().unwrap().callback(|e: InputData| {
            ModelMsg::NewUsername(e.value)
        });

        let onSubmit = link.lock().unwrap().callback(|_| {
            ModelMsg::Click
        });
        websocket.lock().unwrap().set_onmessage(Some(onMessage.as_ref().unchecked_ref()));
        websocket.lock().unwrap().set_onopen(Some(onOpen.as_ref().unchecked_ref()));

        Model {
            link,
            messages: vec!(),
            onMessage,
            onOpen,
            onInput,
            onSubmit,
            onUserChange,
            websocket,
            value: "".to_string(),
            username,
            prev_username,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            ModelMsg::Message(message) => {
                match message {
                    Message::Join(user) => {
                        self.messages.push(format!("Welcome, {}", user))
                    },
                    Message::Leave(user) => {
                        self.messages.push(format!("Goodbye, {}!", user))
                    },
                    Message::SendMessage(msg) => {
                        self.messages.push(format!("{} => {}", msg.owner, msg.content))
                    },
                    Message::ChangeUsername(update) => {
                        self.messages.push(format!("{} is now knows as {}", update.old_name, update.new_name))
                    }
                };
                true
            },
            ModelMsg::Click => {
                if self.username != self.prev_username {
                    let msg = Message::ChangeUsername(ChangeUsername{
                        old_name: self.prev_username.clone(),
                        new_name: self.username.clone(),
                    });
                    let msg = serde_json::to_string(&msg).unwrap();
                    self.websocket.lock().unwrap().send_with_str(&msg).unwrap();
                    self.prev_username = self.username.clone();
                }
                let msg = Message::SendMessage(ChatMessage{
                    owner: self.username.clone(),
                    content: self.value.clone(),
                });
                let msg = serde_json::to_string(&msg).unwrap();
                self.websocket.lock().unwrap().send_with_str(&msg).unwrap();
                self.value = "".to_string();
                false
            },
            ModelMsg::NewValue(value) => {
                self.value = value;
                true
            },
            ModelMsg::NewUsername(value) => {
                self.username = value;
                true
            },
        }
    }

    fn view(&self) -> Html {
        let renderItem = |msg| {
            html! {
                <li>{msg}</li>
            }
        };
        html! {
            <div>
                <ol>
                    { for self.messages.iter().map(renderItem) }
                </ol>
                <input type="text"
                    value=&self.username
                    oninput=self.onUserChange.clone()></input>
                <input type="text"
                    value=&self.value
                    oninput=self.onInput.clone()></input>
                <button onclick=self.onSubmit.clone() disabled={self.value.len() == 0}>{ "Send" }</button>
            </div>
        }
    }
}
